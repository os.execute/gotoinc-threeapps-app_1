class PostsController < ApplicationController
  # before_action :authenticate_user!
  before_action :doorkeeper_authorize!

  def index
    @posts = Post.all
  end

  def create
    post_data = post_params.merge(user: current_user)

    @post = Post.create!(post_data)

    render :show
  end

  def show
    @post = Post.find(params[:id])
  end

  def update
    @post = Post.find(params[:id])
    @post.update!(post_params)

    render :show
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy!

    render :index
  end

  private

  def post_params
    params.require(:post).permit(:title, :body)
  end

  def current_user
    @current_user ||= User.find_by(id: doorkeeper_token[:resource_owner_id])
  end
end
