# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

User.create(email: 'test@test.test', password: '12345678') if Rails.env.development?

post_data = Array.new(22) do
  {
    title: Faker::Book.unique.title,
    body: Faker::Hipster.paragraph(sentence_count: 2),
    user_id: User.order('RANDOM()').first.id
  }
end

Post.insert_all(post_data)
